'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'ngAnimate',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers'
]).
config(['$routeProvider',
 function ($routeProvider) {
        $routeProvider.when('/view0', {
            templateUrl: 'partials/partial0.html',
            controller: 'NavControls',
            title: 'Welcome'
        });
        $routeProvider.when('/view1', {
            templateUrl: 'partials/partial1.html',
            controller: 'MyCtrl1',
            title: 'Benefits'
        });
        $routeProvider.when('/view2', {
            templateUrl: 'partials/partial2.html',
            controller: 'MyCtrl2',
            title: 'Data Binding'
        });
        $routeProvider.when('/view3', {
            templateUrl: 'partials/partial3.html',
            controller: 'NavControls',
            title: 'Todo List'
        });
        $routeProvider.when('/view4', {
            templateUrl: 'partials/partial4.html',
            controller: 'NavControls',
            title: 'Animations'
        });
        $routeProvider.when('/view5', {
            templateUrl: 'partials/partial5.html',
            controller: 'NavControls',
            title: 'SPI Glass'
        });
        $routeProvider.when('/view6', {
            templateUrl: 'partials/partial6.html',
            controller: 'NavControls',
            title: 'Q & A'
        });
        $routeProvider.when('/view7', {
            templateUrl: 'partials/partial7.html',
            controller: 'NavControls',
            title: 'Fork Me!'
        });
        $routeProvider.otherwise({
            redirectTo: '/view0'
        });
}]).run(['$location', '$rootScope',
 function ($location, $rootScope) {
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            $rootScope.title = current.$$route.title;
        });
}]);