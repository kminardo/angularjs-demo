'use strict';

/* Directives */


angular.module('myApp.directives', []).
directive('appVersion', ['version',
	function (version) {
		return function (scope, elm, attrs) {
			elm.text(version);
		};
  }]).directive('keypressEvents', ['$document', '$rootScope', function ($document, $rootScope) {
		return {
			restrict: 'A',
			link: function () {
				$document.bind('keydown', function (e) {
					$rootScope.$broadcast('keypress', e);
					$rootScope.$broadcast('keypress:' + e.which, e);
				});
			}
		};
  }
]);