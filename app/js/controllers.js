'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
    .controller('MyCtrl1', ['$scope', '$location', '$rootScope',
  function ($scope, $location, $rootScope) {
            //Navigation Controls
            $scope.$on('keypress:39', function (onEvent, keypressEvent) {
                var path = $location.path();
                path = path.substr(path.length - 1);
                path++;
                $rootScope.$apply(function () {
                    $location.path('/view' + path);
                });
            });

            $scope.$on('keypress:37', function (onEvent, keypressEvent) {
                var path = $location.path();
                path = path.substr(path.length - 1);
                path--;
                $rootScope.$apply(function () {
                    $location.path('/view' + path);
                });
            });

            //Variables
            $scope.listContent = [
     "Lightweight Framework",
     "100% JavaScript & 100% Client Side solution",
     "Create Dynamic Views in HTML with auto + bidirectional Data Binding",
     "Works as a templating system, which is compiled into the browser to produce a live view.",
     "HTML Template & JavaScript are separated",
     "Templates register controllers, controllers are DOM independent",
     "Ease of animating page controls and actions",
     "Dependency Injection support",
     "Angular Widgets Backed by Google Extensive community and documentation"
     ]

            //Functions

            //Internal Functions			

  }])
    .controller('MyCtrl2', ['$scope', '$location', '$rootScope',
  function ($scope, $location, $rootScope) {
            //Navigation Controls
            $scope.$on('keypress:39', function (onEvent, keypressEvent) {
                var path = $location.path();
                path = path.substr(path.length - 1);
                path++;
                $rootScope.$apply(function () {
                    $location.path('/view' + path);
                });
            });

            $scope.$on('keypress:37', function (onEvent, keypressEvent) {
                var path = $location.path();
                path = path.substr(path.length - 1);
                path--;
                $rootScope.$apply(function () {
                    $location.path('/view' + path);
                });
            });

            //Variables
            $scope.lastName = "Minardo";

            //Functions
            $scope.nextPage = function () {
                $location.path('/view3');
            };

            //Internal Functions			

  }])
    .controller('NavControls', ['$scope', '$location', '$rootScope',
  function ($scope, $location, $rootScope) {
            //Navigation Controls
            $scope.$on('keypress:39', function (onEvent, keypressEvent) {
                var path = $location.path();
                path = path.substr(path.length - 1);
                path++;
                $rootScope.$apply(function () {
                    $location.path('/view' + path);
                });
            });

            $scope.$on('keypress:37', function (onEvent, keypressEvent) {
                var path = $location.path();
                path = path.substr(path.length - 1);
                path--;
                $rootScope.$apply(function () {
                    $location.path('/view' + path);
                });
            });

            //Variables

            //Functions

            //Internal Functions			

  }])
    .controller('TodoController', ['$scope', '$location', '$rootScope',
  function ($scope) {
            //Variables
            var todos = $scope.todos = JSON.parse(localStorage.getItem('todoListStorage') || '[]');

            //Functions
            $scope.addTodo = function () {
                var newTodo = $scope.todoText.trim();
                if (newTodo.length === 0) {
                    return;
                }

                todos.push({
                    text: newTodo,
                    done: false
                });

                localStorage.setItem('todoListStorage', JSON.stringify(todos));
                $scope.todoText = "";
            };

            $scope.remaining = function () {
                var count = 0;
                angular.forEach($scope.todos, function (todo) {
                    count += todo.done ? 0 : 1;
                });
                return count;
            };

            $scope.archive = function () {
                var oldTodos = todos;
                todos = [];
                angular.forEach(oldTodos, function (todo) {
                    if (!todo.done) todos.push(todo);
                });
                $scope.todos = todos;
                localStorage.setItem('todoListStorage', JSON.stringify(todos));
            };
      
            //Internal Functions			

  }]);